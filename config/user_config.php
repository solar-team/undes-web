<?php

return [
    'menus' => [

        [
            'name' => 'Миний баг',
            'icon' => 'icon-user',
            'url' => 'javascript:;',
            'child' => [
                [
                    'name' => 'Бинар',
                    'icon' => '',
                    'url' => '/user'
                ],
                [
                    'name' => 'Олон үет',
                    'icon' => '',
                    'url' => '/user/multiLevel'
                ]
            ]
        ],
        [
            'name' => 'Гишүүнчлэлийн цалин',
            'icon' => 'icon-diamond',
            'url' => 'javascript:;',
            'child' => [
                [
                    'name' => 'Хураангуу',
                    'icon' => '',
                    'url' => '/user/binarySalary'
                ],
                [
                    'name' => 'Дэлгэрэнгүй',
                    'icon' => '',
                    'url' => '/user/binarySalarylist'
                ],
            ]
        ],
        [
            'name' => 'Худалдааны урашуулал',
            'icon' => 'fa fa-money',
            'url' => 'javascript:;',
            'child' => [
                [
                    'name' => 'Хураангуу',
                    'icon' => '',
                    'url' => '/user/binaryMultiLevelSalary'
                ],
                [
                    'name' => 'Дэлгэрэнгүй',
                    'icon' => '',
                    'url' => '/user/binaryMultiLevelSalaryList'
                ],
            ]
        ]
    ]
];