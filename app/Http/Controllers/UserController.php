<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use View;
use Solarcms\TableProperties\TableProperties;
use Solarcms\Core\TableProperties\Tp\Tp;
use App\User;
use Mail;
use Session;

class UserController extends Controller
{
    protected $firstname = '';
    protected $userId = null;

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('role:2');
        if(Auth::user()){
            $this->firstname = Auth::user()->firstname;
            $this->userId = Auth::user()->id;
            View::share('firstname', $this->firstname); 
        }


    }

    public function TableProperties($slug, $action = 'index')
    {

        if (!method_exists($this, $slug)) {
            return "nope";
        } else {
            return $this->$slug($action);
        }
    }

    public function getBinaryTree()
    {
        $tree = DB::select("call getTreeFrom($this->userId)");
        return $tree;
    }

    public function index()
    {

        $binaryTree = $this->getBinaryTree();


        return view('user._pages.treeBinary', compact('binaryTree'));
    }

    public function newUser($parent_id, $pside, $action = 'index')
    {
        $tp = new tp();
        $tp->viewName = 'user._pages.only_tp';
        $tp->table = 'users';

        $tp->permission = ['c' => true, 'r' => false, 'u' => false, 'd' => false];

        $tp->page_name = 'Гишүүд';
        $tp->save_button_text = 'Бүртгэх';
        $tp->identity_name = 'id';
        $tp->grid_columns = ['users.name', 'users.email', 'users.id'];
        $tp->grid_default_order_by = 'users.id DESC';
        $tp->formType = 'page';
        $tp->where_condition = [

        ];
        $tp->hidden_values = [
            'parent_id'=>$parent_id,
            'birthdate'=>null,
            'gender'=>null,
            'email'=>null,
            'bank_id'=>null,
            'dans'=>null,
            'aimag_id'=>null,
            'sum_id'=>null,
            'bag_id'=>null,
            'address'=>null,
            'qrcode'=>null,

        ];
        $tp->created_at = 'created_at';
        $tp->updated_at = 'updated_at';
        $tp->formClassName = 'user-form col-sm-12 col-md-8 col-lg-12';

        $tp->grid_output_control = [
            ['column' => 'name', 'title' => 'Гишүүний нэр', 'type' => '--text'],
            ['column' => 'email', 'title' => 'Имэйл', 'type' => '--email']
        ];

        $parent_side = null;


        $tp->form_input_control = [
            ['column' => 'username', 'title' => 'Регистерийн дугаар', 'type' => '--text', 'value' => null,  'validate'=>'required|unique:users,username,NULL,'.$tp->identity_name],
            ['column' => 'lastname', 'title' => 'Овог', 'type' => '--text', 'value' => null, 'validate' => 'required'],
            ['column' => 'firstname', 'title' => 'Нэр', 'type' => '--text', 'value' => null, 'validate' => 'required'],
            ['column' => 'birthdate', 'title' => 'Төрсөн өдөр', 'type' => '--date', 'value' => null, 'validate' => 'required'],
            ['column'=>'gender', 'title'=>'Хүйс', 'type'=>'--radio', 'value'=>0, 'choices'=>[
                ['value'=>0, 'text'=>'Эрэгтэй'],
                ['value'=>1, 'text'=>'Эмэгтэй'],
            ], 'validate'=>'required'],
            ['column' => 'email', 'title' => 'И-мэйл', 'type' => '--email', 'value' => null,  'validate'=>'required|email|unique:users,email,NULL,'.$tp->identity_name],
            ['column' => 'bank_id', 'title' => 'Банк', 'type' => '--combobox', 'value' => null, 'validate' => 'required', 'options' => [
                'valueField' => 'id',
                'textField' => 'bankname',
                'table' => 'bank',
                'identity_name' => 'id',
                'grid_columns' => ['id', 'bankname'],
                'grid_default_order_by' => 'id ASC',
            ]],
            ['column' => 'dans', 'title' => 'Дансны дугаар', 'type' => '--number', 'value' => null,  'validate'=>'required|unique:users,dans,NULL,'.$tp->identity_name],
            ['column'=>'aimag_id', 'title'=>'Аймаг, нийслэл', 'type'=>'--combobox', 'value'=>null, 'validate'=>'required', 'options'=>[
                'valueField'=> 'id',
                'textField'=> 'aimagname',
                'table'=>'aimag',
                'identity_name'=>'id',
                'grid_columns'=>['id', 'aimagname'],
                'grid_default_order_by'=>'aimagname ASC',
                'child'=>'sum_id',
            ]],
            ['column'=>'sum_id', 'title'=>'Сум/ дүүрэг', 'type'=>'--combobox', 'value'=>null, 'validate'=>'required', 'options'=>[
                'parent'=>'aimagid',
                'valueField'=> 'id',
                'textField'=> 'sumname',
                'table'=>'sum',
                'identity_name'=>'id',
                'grid_columns'=>['id', 'sumname'],
                'grid_default_order_by'=>'sumname ASC',
                'child'=>'bag_id',
            ]],
            ['column'=>'bag_id', 'title'=>'Баг/ хороо', 'type'=>'--combobox', 'value'=>null, 'validate'=>'required', 'options'=>[
                'parent'=>'sumid',
                'valueField'=> 'id',
                'textField'=> 'bagname',
                'table'=>'bag',
                'identity_name'=>'id',
                'grid_columns'=>['id', 'bagname'],
                'grid_default_order_by'=>'bagname ASC'
            ]],
            ['column' => 'address', 'title' => 'Гудамж, хороолол, байр, байшин', 'type' => '--textarea', 'value' => null, 'validate' => 'required'],

            ['column' => 'phone', 'title' => 'Утас', 'type' => '--text', 'value' => null,  'validate'=>'required'],
            ['column' => 'qrcode', 'title' => 'Ирэгнйи үнэмлэхний дугаар', 'type' => '--number', 'value' => null,  'validate'=>'required|unique:users,qrcode,NULL,'.$tp->identity_name],
        ];

        if($pside == 'null'){
            $user_sides = DB::table('users')->select('id', 'username', 'pside')->where('parent_id', $parent_id)->get();
            if(count($user_sides) >= 2){
                return "Хүн нэмэх боломжгүй";
            } else {
                array_unshift($tp->form_input_control, ['column'=>'pside', 'title'=>'Спонсорын аль гар', 'type'=>'--radio', 'value'=>'r', 'choices'=>[
                    ['value'=>'r', 'text'=>'A'],
                    ['value'=>'l', 'text'=>'Б'],
                ], 'validate'=>'required']);
            }

        } else {
            $user_sides = DB::table('users')->select('id', 'username', 'pside')->where('parent_id', $parent_id)->get();

            if(count($user_sides) >= 2){
                return "Хүн нэмэх боломжгүй";
            } else {
                foreach ($user_sides as $user_side){
                    if($user_side->pside == $pside){
                        return "Энэ талд хүн нэмэх боломжгүй";
                    }
                }
            }

           $tp->hidden_values['pside']=$pside;
        }
        $tp->before_insert = [
            'controller'=>'App\Http\Controllers\UserController',
            'function'=>'beforeInsertUser',
            'arguments'=>[]
        ];

        $tp->insert_response_function = [
            'controller'=>'App\Http\Controllers\UserController',
            'function'=>'createResponseUser',
        ];
        $tp->show_insert_response = true;
 
        return $tp->run($action);
    }
    public function beforeInsertUser($data){
        $insert_values = $data['insert_values'];

        $user = [];

//        $pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
            $pass= "pass#123";

        Session::set('showPass', $pass);
        Session::set('pside', $insert_values['pside']);
        Session::set('parent_id', $insert_values['parent_id']);

        $tree_level = DB::table('users')->select('tree_level')->where('id', $insert_values['parent_id'])->first();
        $tree_level = $tree_level->tree_level;

        return ['password'=>bcrypt($pass), 'tree_level' => $tree_level + 1, 'ruser'=>0, 'luser'=>0, 'active'=>1];
    }
    public function createResponseUser($data){
        $pass= Session::get('showPass');
        $pside= Session::get('pside');
        $parent_id= Session::get('parent_id');
        Session::set('showPass', '');

        $newUserId = DB::getPdo()->lastInsertId();

        if ($pside == 'r') {
            DB::table('users')->where('id', $parent_id)->update(['ruser' => $newUserId]);

        } elseif ($pside == 'l') {
            DB::table('users')->where('id', $parent_id)->update(['luser' => $newUserId]);
        }

        return "Гишүүний нууц үг: $pass";


    }

    public function changePassword($action = 'index')
    {

        $tp = new Tp();
        $tp->viewName = 'user._pages.tp';
        $tp->table = 'users';
        $tp->password_change = true;
        $tp->page_name = 'Нууц үг солих';
        $tp->identity_name = 'id';
        $tp->grid_columns = ['users.id'];
        $tp->grid_default_order_by = 'users.id DESC';
        $tp->formType = 'page';
        $tp->created_at = 'created_at';
        $tp->updated_at = 'updated_at';

        $tp->permission = ['c' => false, 'r' => false, 'u' => true, 'd' => false];
        $tp->update_row = Auth::user()->id;


        $tp->grid_output_control = [

        ];

        $tp->form_input_control = [
            ['column' => 'password', 'title' => 'Нууц үг', 'type' => '--password', 'value' => null, 'validate' => 'required|'],
            ['column' => 'password_confirm', 'title' => 'Нууц үг баталгаажлах', 'type' => '--password-confirm', 'value' => null, 'validate' => ''],
        ];


        return $tp->run($action);


    }
    
    public function userSalary($user_id)
    {

        $user = DB::table('users')->where('id', $user_id)->first();

        $treeA = DB::select("call getTreeFrom($user->ruser)");
        $treeB = DB::select("call getTreeFrom($user->luser)");

        return ['A'=>count($treeA), 'B'=>count($treeB)];

    }
    public function allSalary(){

        $users=DB::table('users')->where('role_id', 2)->get();


        foreach ($users as $user){
            $userChild = $this->userSalary($user->id);

            $treeA =$userChild['A'];
            $treeB = floor($userChild['B']);

            $a5 = floor($treeA/5);
            $b3 = floor($treeB/3);

            $salary = 0;
            if($treeB>=3 & $treeA >=5){
                if($a5 > $b3){
                    $salary = $b3*40000;
                } else if($b3 > $a5){
                    $salary = $a5*40000;
                }

                $insert = [
                    'user_id'=>$user->id,
                    'username'=>$user->username,
                    'a_users'=>$treeA,
                    'b_users'=>$treeB,
                    'salary'=>$salary,
                ];

                DB::table('user_salary')->insert($insert);

                echo $user->id."<br/>";
            } else {
                $insert = [
                    'user_id'=>$user->id,
                    'username'=>$user->username,
                    'a_users'=>$treeA,
                    'b_users'=>$treeB,
                    'salary'=>0,
                ];

                DB::table('user_salary')->insert($insert);

                echo $user->id."<br/>";
            }



        }



    }


    public function fixDans(){
        $users=DB::table('users')->where('role_id', 2)->get();


        foreach ($users as $user){

                $old = DB::table('mbm2_users')->select('bank_account')->where('id', $user->old_id)->first();
            if($old){
                $insert = [
                    'dans'=>$old->bank_account,

                ];

                DB::table('users')->where('id', $user->id)->update($insert);
            }






        }
    }


}
