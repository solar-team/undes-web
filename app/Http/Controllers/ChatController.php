<?php

namespace App\Http\Controllers;



use Illuminate\Support\Facades\DB;
use Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{


    public function userChat($user_id){
        $last_chat_room = DB::table('chat')->where('sender_id', $user_id)->orWhere('receiver_id', $user_id)->orderBy('id', 'ASC')->get();

        DB::table('chat')->where('sender_id', $user_id)->update(['seen'=>1]);


        return $last_chat_room;
    }
    public function chatNew($user_id){

        DB::table('chat')->where('receiver_id', $user_id)->update(['seen'=>1]);

        $last_chat_room = DB::table('chat')->where('sender_id', $user_id)->orWhere('receiver_id', $user_id)->take(21)->orderBy('id', 'DESC')->get();

        return $last_chat_room;
    }
    public function chatOther($user_id){
        $last_chat_room = DB::table('chat_view')
            ->select('sender_id', 'chat', DB::raw("count(id) as total_un_seen"))
            ->groupBy('sender_id')
            ->where('sender_id', '!=', $user_id)
            ->where('receiver_id', 1)->where('seen', 0)
            ->orderBy('total_un_seen', 'ASC')

            ->get();

        return $last_chat_room;
    }
    public function chatInfo($user_id){
        $last_chat_room = DB::table('chat_view')
            ->select('chat', DB::raw("count(id) as total_un_seen"))
            ->groupBy('receiver_id')
            ->where('receiver_id', $user_id)->where('seen', 0)
            ->orderBy('total_un_seen', 'ASC')

            ->get();

        return $last_chat_room;
    }

    public function sendUserChat($user_id){
        $message = Request::input('message');

        $insert = DB::table('chat')->insert(['chat'=>$message, 'created_at'=>\Carbon\Carbon::now(), 'sender_id'=>$user_id, 'receiver_id'=>1]);

        if($insert)
            return DB::getPdo()->lastInsertId();
        else
            return 'error';
    }
}
