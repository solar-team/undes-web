<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class CheckController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function check (){



        $role = User::UserRole();

      

        switch($role){
            case 1:  return redirect('/admin/user'); break;
            case 2:  return redirect('/operator/hicheeldHamragdah'); break;
            case 3:  return redirect('/siteadmin/content'); break;
            case 4:  return redirect('/hoperator/user'); break;
            case 5:  return redirect('/user/deglem'); break;
            default:  return redirect('/logout');
        }


    }

    public function goChangePassword (){



        $role = User::UserRole();

        switch($role){
            case 1:  return redirect('/admin/changePassword/'); break;
            case 2:  return redirect('/operator/changePassword/'); break;
            case 3:  return redirect('/siteadmin/changePassword/'); break;
            case 4:  return redirect('/hoperator/changePassword/'); break;
            case 5:  return redirect('/user/changePassword/'); break;
            default:  return redirect('/logout');
        }


    }
}
