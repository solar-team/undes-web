<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TulburConroller extends Controller
{
    public function genrateAndSetTultur(){
        $users = DB::table('users')->select('id','email', 'daraagiin_tulult')->where('role_id','=',5)->get();
        $tulburs = [];
        
        foreach ($users as $user){
            
            $tulbur = DB::table('tulbur')->select('honog', 'tulsun_ogno')
                ->where('user_id', '=', $user->id)
                ->orderBy('tulsun_ogno', 'DESC')
                ->first();

            $today = date_format(\Carbon\Carbon::now(), "Y-m-d");

            $chuluu = DB::table('chuluu')
                ->where('ehleh_udur', '<=', $today)
                ->where('user_id', '=', $user->id)
                ->orderBy('ehleh_udur', 'DESC')->first();

            if($chuluu){

            } else {
                $tulburs[] = $tulbur;
            }

            
        }
        
        return $tulburs;
    }
}
