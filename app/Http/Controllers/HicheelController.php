<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HicheelController extends Controller
{
    public function hicheeldHamragdsan($on, $sar)
    {

        $days = cal_days_in_month(CAL_GREGORIAN, $sar, $on);

        $data = [];
        $header = ['Гишүүн', 'Код', 'Ирсэн'];
        $column = [
            [
                'data' => 'fullname',
                'type' => 'text',
                'editor' => false
            ],
            [
                'data' => 'bar_code',
                'type' => 'text',
                'editor' => false
            ],
            [
                'data' => 'total_irsen',
                'type' => 'text',
                'editor' => false
            ]
        ];


        $report = DB::table('hicheel_report_view')->select('fullname', 'bar_code', 'total_irsen')
            ->where('year', $on)
            ->where('month', $sar);


        $report_plus = DB::table('hicheel_report')
            ->select('user_id as fullname', 'total_irsen as bar_code', 'total_irsen')
            ->where('year', 2016)
            ->where('month', '08')->orderBy('user_id', 'ASC')->take(4);

        for ($i = 1; $i <= $days; $i++) {

            $header[] = $i;
            $this_col = 'd';

            if($i < 10)
                $this_col = $this_col."_0".$i;
            else
                $this_col = $this_col."_".$i;

            $column[] = [
                'data' => $this_col,
                'type' => 'text',
                'editor' => false
            ];
            $report->addSelect($this_col);
            $report_plus->addSelect($this_col);

        }

        $data0 = $report->orderBy('name', 'ASC')->get();
        $report_plus0 = $report_plus->get();

        $data = array_merge($data0, $report_plus0);

        return ['data' => $data, 'header' => $header, 'column' => $column, 'count_user' => count($data0)];

    }
    public function hicheeldHamragdsanOld($on, $sar)
    {

        $days = cal_days_in_month(CAL_GREGORIAN, $sar, $on);

        $data = [];
        $header = ['Гишүүн', 'Код', 'Ирсэн'];
        $column = [
            [
                'data' => 'name',
                'type' => 'text',
                'editor' => false
            ],
            [
                'data' => 'bar_code',
                'type' => 'text',
                'editor' => false
            ],
            [
                'data' => 'niit_irsen',
                'type' => 'text',
                'editor' => false
            ]
        ];

        for ($i = 1; $i <= $days; $i++) {

            $header[] = $i;

            $column[] = [
                'data' => $i,
                'type' => 'text',
                'editor' => false
            ];

        }
        $users = DB::table('users')->select('id', 'lastname', 'name', 'bar_code')->where('role_id', '=', 5)->orderBy('name', 'ASC')->get();

//        dd($users);

        $sum_niit_irsen = 0;
        $data_sum_day = ['name' => '* Нийт ирcэн өдрөөр:', 'bar_code' => '', 'niit_irsen'=>''];
        $data_sum_day_un_pay = ['name' => '* Төлбөр төлөөгүй:', 'bar_code' => '', 'niit_irsen'=>''];
        $data_sum_day_pay = ['name' => '* Төлбөр төлөсөн:', 'bar_code' => '', 'niit_irsen'=>''];

        foreach ($users as $user) {
            $name = mb_substr($user->lastname, 0, 1, 'utf-8') . "." . $user->name;


            $data_pre = ['name' => $name, 'bar_code' => $user->bar_code];

            $niit_irsen = 0;
            $tuluugui = 0;

//            dd($data_pre);
            foreach ($header as $heead) {
                if ($heead != 'Гишүүн' && $heead != 'Код' && $heead != 'Ирсэн') {

                    $date = "$on-$sar-$heead";

                    $hicheel_info = DB::table('hicheeld_hamragdsan')
                        ->select('tulbur_tulsun_eseh')
                        ->where('ognoo', '=', $date)
                        ->where('user_id', '=', $user->id)
                        ->first();
                    if ($hicheel_info) {

                        $niit_irsen++;

                        $data_pre[$heead] = $hicheel_info->tulbur_tulsun_eseh;
                    } else
                        $data_pre[$heead] = null;

                }

            }
            $data_pre['niit_irsen'] = $niit_irsen;

            $sum_niit_irsen = $sum_niit_irsen + $niit_irsen;


            $data[] = $data_pre;

        }

        foreach ($data as $data_) {

            foreach ($data_ as $data_key => $data_value) {

                if ($data_key != 'bar_code' && $data_key != 'name' && $data_key != 'niit_irsen'){

                    if (isset($data_sum_day[$data_key])) {

                        if ($data_value === 0 || $data_value == 1) {

                            $data_sum_day[$data_key] = $data_sum_day[$data_key] + 1;

                            if($data_value === 0){
                                $data_sum_day_un_pay[$data_key] = $data_sum_day_un_pay[$data_key] + 1;
                            } else {
                                $data_sum_day_pay[$data_key] = $data_sum_day_pay[$data_key]+1;
                            }
                        }

                    } else {

                        $data_sum_day_un_pay[$data_key] = 0;
                        $data_sum_day_pay[$data_key] =0;

                        if ($data_value === 0 || $data_value == 1) {
                            $data_sum_day[$data_key] = 1;

                            if($data_value === 0){
                                $data_sum_day_un_pay[$data_key] = 1;
                            } else {
                                $data_sum_day_pay[$data_key] = 1;
                            }
                        } else {
                            $data_sum_day[$data_key] = 0;
                        }

                    }
                }

            }

        }

        $data[] = $data_sum_day;
        $data[] = $data_sum_day_pay;
        $data[] = $data_sum_day_un_pay;

        $data_sum = ['name' => '* Нийт ирэх ёстой:', 'bar_code' => '', 'niit_irsen' => count($users)];

        $data[] = $data_sum;


        $data2_sum = ['name' => '* Нийт ирсэн:', 'bar_code' => '', 'niit_irsen' => $sum_niit_irsen];
        $data[] = $data2_sum;

        return ['data' => $data, 'header' => $header, 'column' => $column, 'count_user' => count($users)];
    }


    public function hicheelCron($date = null){


        if($date){

            $year = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('Y');
            $month = \Carbon\Carbon::createFromFormat('Y-m-d',$date)->format('m');
            $day = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d');
            $dayFull = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('D');

            $date_hyazgaar = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->subDays(30)->format('Y-m-d');
        } else {
            $year = \Carbon\Carbon::today()->format('Y');
            $month = \Carbon\Carbon::today()->format('m');
            $day = \Carbon\Carbon::today()->format('d');

            $dayFull = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('D');

            $date_hyazgaar = \Carbon\Carbon::today()->subDays(30)->format('Y-m-d');
        }








//        $dayFull
        /* Mon, Tue, Wed, Thu, Fri, Sat, Sun */
        $ireh_yostoi = 0;
        if($dayFull == 'Mon' || $dayFull == 'Wed'|| $dayFull == 'Fri'){
            $ireh_yostoi_huuchin = DB::table('users')->where('elssen_ognoo', '<', $date_hyazgaar)->where('role_id', 5)->count();
            $ireh_yostoi = $ireh_yostoi_huuchin;
        }elseif($dayFull == 'Tue' || $dayFull == 'Thu'|| $dayFull == 'Sat'){
            $ireh_yostoi_shine = DB::table('users')->where('elssen_ognoo', '>=', $date_hyazgaar)->where('role_id', 5)->count();
            $ireh_yostoi = $ireh_yostoi_shine;
        } elseif($dayFull == 'Sun'){
            $ireh_yostoi = 0;
        }


       $irsen = DB::table('hicheeld_hamragdsan')->where('ognoo', $date)->count();

        $tulsun =  DB::table('hicheeld_hamragdsan')->where('ognoo', $date)->where('tulbur_tulsun_eseh', '1')->count();
        $tuluugui =  DB::table('hicheeld_hamragdsan')->where('ognoo', $date)->where('tulbur_tulsun_eseh', '0')->count();


        $check_ireh = DB::table('hicheel_report')
            ->select('id', 'total_irsen')
            ->where('year', $year)
            ->where('month', $month)
            ->where('user_id', '-1')->first();

        $check_irsen = DB::table('hicheel_report')
            ->select('id', 'total_irsen')
            ->where('year', $year)
            ->where('month', $month)
            ->where('user_id', '-2')->first();

        $check_tuluugui = DB::table('hicheel_report')
            ->select('id', 'total_irsen')
            ->where('year', $year)
            ->where('month', $month)
            ->where('user_id', '-3')->first();

        $check_tulsun = DB::table('hicheel_report')
            ->select('id', 'total_irsen')
            ->where('year', $year)
            ->where('month', $month)
            ->where('user_id', '-4')->first();

        if($check_ireh)
            DB::table('hicheel_report')->where('id', $check_ireh->id)->update(["d_$day"=>$ireh_yostoi]);
        else
            DB::table('hicheel_report')
                ->insert([
                    "d_$day"=>$ireh_yostoi,
                    'user_id'=>'-1',
                    'year'=>$year,
                    'month'=>$month
                ]);

        if($check_irsen)
            DB::table('hicheel_report')->where('id', $check_irsen->id)->update(["d_$day"=>$irsen]);
        else
            DB::table('hicheel_report')
                ->insert([
                    "d_$day"=>$irsen,
                    'user_id'=>'-2',
                    'year'=>$year,
                    'month'=>$month
                ]);

        if($check_tuluugui)
            DB::table('hicheel_report')->where('id', $check_tuluugui->id)->update(["d_$day"=>$tuluugui]);
        else
            DB::table('hicheel_report')
                ->insert([
                    "d_$day"=>$tuluugui,
                    'user_id'=>'-3',
                    'year'=>$year,
                    'month'=>$month
                ]);

        if($check_tulsun)
            DB::table('hicheel_report')->where('id', $check_tulsun->id)->update(["d_$day"=>$tulsun]);
        else
            DB::table('hicheel_report')
                ->insert([
                    "d_$day"=>$tulsun,
                    'user_id'=>'-4',
                    'year'=>$year,
                    'month'=>$month
                ]);



    }
    public function hicheeldDo(){



        $dates = DB::table('dates')->where('date', '>=', '2016-04-01')->get();


        foreach ($dates as $date){
            $this->hicheelCron($date->date);
        }

        dd('bolson');

    }


}
