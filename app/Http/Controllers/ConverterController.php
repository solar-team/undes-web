<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Hash;

class ConverterController extends Controller
{
    public function getUsers()
    {

        dd(Hash::make('pass#123'));
//        $oldUsers = DB::table('mbm2_users')
//            ->select('sponsor_username', 'username', 'email', 'firstname', 'lastname', 'bank', 'bank_account', 'mobile')
//            ->where('id', '>=', 20025)
//            ->get();
//
//        dd($oldUsers);
    }

    public function addUser()
    {


        $oldUsers = DB::table('mbm2_users')
            ->select('id', 'user_id', 'sponsor_username', 'username', 'email', 'firstname', 'lastname', 'bank', 'bank_account', 'mobile')
            ->where('id', '>=', 20026)
            ->orderBy('id', 'ASC')
            ->get();

//        dd($oldUsers);

        foreach ($oldUsers as $oldUser) {

            if ($oldUser->id != 20026 && $oldUser->id != 20033) {
                $pSide = 'r';
                $parent_id = DB::table('users')->select('id')->where('old_id', $oldUser->user_id)->first();
                if($parent_id){
                    $parent_id = $parent_id->id;

                    $childs = DB::table('users')->select('id')->where('parent_id', $parent_id)->get();

                    if(count($childs) == 0){
                        $pSide = 'r';
                    } else {
                        $pSide = 'l';
                    }

                    $UserData = [
                        'active' => 1,
                        'aimag_id' => 1,
                        'sum_id' => 1,
                        'bag_id' => 1,
                        'username' => $oldUser->username,
                        'email' => null,
                        'qrcode' => null,
                        'firstname' => $oldUser->firstname,
                        'lastname' => $oldUser->lastname,
                        'birthdate' => null,
                        'gender' => null,
                        'phone' => $oldUser->mobile,
                        'bank_id' => 1,
                        'dans' => $oldUser->bank_account,
                        'address' => null,
                        'user_id' => $oldUser->user_id,
                        'old_id' => $oldUser->id,
                        'password' => '$2y$10$SSj.VawrpuShYnUMlABPXuR3kJ0aiFelob92YwL1xagamCh/Y4RGW',
                    ];
                    $nId = $this->addUserTree($parent_id, $pSide, $UserData);
                    echo "<div>$nId, $oldUser->id</div>";
                } else {
                    DB::table('buruu_users')->insert(['sponsor_username'=>$oldUser->sponsor_username, 'username'=>$oldUser->username, 'user_id'=>$oldUser->user_id, 'old_id'=>$oldUser->id]);
                }

            } else{


            }

        }

        return "<h1>Duuslaa</h1>";

    }

    public function addUserTree($parent_id, $pSide, $UserData)
    {

        $iCheck = false;
        $iLevel = 0;

        $savedUser = 0;

        if ($pSide == 'r') {
            $savedUser = DB::table('users')->select('ruser')->where('id', $parent_id)->first();
            $savedUser = $savedUser->ruser;

        } elseif ($pSide == 'l') {
            $savedUser = DB::table('users')->select('luser')->where('id', $parent_id)->first();
            $savedUser = $savedUser->luser;
        }


        if ($savedUser == 0) {
            $tree_level = DB::table('users')->select('tree_level')->where('id', $parent_id)->first();
            $tree_level = $tree_level->tree_level;

            $newUser = [
                'active' => $UserData['active'],
                'parent_id' => $parent_id,
                'pside' => $pSide,
                'ruser' => 0,
                'luser' => 0,
                'tree_level' => $tree_level + 1,
                'aimag_id' => $UserData['aimag_id'],
                'sum_id' => $UserData['sum_id'],
                'bag_id' => $UserData['bag_id'],
                'username' => $UserData['username'],
                'email' => $UserData['email'],
                'qrcode' => $UserData['qrcode'],
                'firstname' => $UserData['firstname'],
                'lastname' => $UserData['lastname'],
                'birthdate' => $UserData['birthdate'],
                'gender' => $UserData['gender'],
                'phone' => $UserData['phone'],
                'bank_id' => $UserData['bank_id'],
                'dans' => $UserData['dans'],
                'address' => $UserData['address'],
                'password' => $UserData['password'],
                'user_id' => $UserData['user_id'],
                'old_id' => $UserData['old_id'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            DB::table('users')->insert($newUser);
            $newUserId = DB::getPdo()->lastInsertId();

            if ($pSide == 'r') {
                DB::table('users')->where('id', $parent_id)->update(['ruser' => $newUserId]);

            } elseif ($pSide == 'l') {
                DB::table('users')->where('id', $parent_id)->update(['luser' => $newUserId]);
            }

            return $newUserId;


        } else {
            return 'saved';
        }


    }
    
    
  
}
