<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/login',
        '/admin/chat',
        '/user/chat',
        '/send-user-chat/*',
        '/send-user-chat',
        '/chat',
        '/logout',
        '/password/*',
        '/api/*',
    ];
}
