<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
header("Access-Control-Allow-Origin: *");

Route::get('/', function () {
   return redirect('/login');
});
Route::get('/hicheeld-hamragdsan/{on}/{sar}', 'HicheelController@hicheeldHamragdsan');
Route::get('/hicheeldDo', 'HicheelController@hicheeldDo');
Route::get('/check', 'CheckController@check');
Route::get('/go-change-password', 'CheckController@goChangePassword');


Route::auth();

Route::get('/chat/{user_id}', 'ChatController@userChat');
Route::post('/send-user-chat/{user_id}', 'ChatController@sendUserChat');
Route::get('/chat-info/{user_id}', 'ChatController@chatInfo');
Route::get('/chatNew/{user_id}', 'ChatController@chatNew');
Route::get('/chatOther/{user_id}', 'ChatController@chatOther');




