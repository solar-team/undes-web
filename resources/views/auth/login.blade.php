@extends('auth.layout')

@section('content')

                  <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}
                        @if ($errors->has('username') || $errors->has('password'))

                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                @if ($errors->has('username'))

                                    <strong>{{ $errors->first('username') }}</strong>
                                @endif
                                @if ($errors->has('password'))

                                        <strong>{{ $errors->first('password') }}</strong>

                                @endif

                            </div>
                        @endif

                        <div class="row">
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Регистерийн дугаар" name="username" value="{{ old('username') }}" required/> </div>
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Нууц үг" name="password" required/> </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="rem-password">
                                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox" name="remember" value="1" /> Намайг сана
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-8 text-right">
                                <div class="forgot-password">
                                    <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Нууц үгээ мартсан?</a>
                                </div>
                                <button class="btn green" type="submit">Нэвтрэх</button>
                            </div>
                        </div>
                    </form>
    
    
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <br>
            <h4>Аппликейшнээ татаж аваарай</h4>
            <a href="https://play.google.com/store/apps/details?id=com.phonegap.undes" style="text-align: center; text-decoration: none; display: block">
                <img src="/assets/img/appstore.png" alt="" width="150"/>

                <br>
                Andriod

                <br>
                <p>IOS хувилбар тун удахгүй</p>
        </div>
    </div>

@endsection
