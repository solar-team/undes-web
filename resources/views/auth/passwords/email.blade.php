@extends('auth.layout')

<!-- Main Content -->
@section('content')

<form class="forget-form" role="form" method="POST" action="{{ url('/password/email') }}" style="display: block">
    {!! csrf_field() !!}
    <h3 class="font-green">Нууц үг шинэчлэх ?</h3>
    <p>И-мэйл хаягаа оруулаад илгээх товчлуурыг дарна уу. </p>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="form-group">

        <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="И-мэйл" name="email" value="{{ old('email') }}" />

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-actions">

        <button type="submit" class="btn btn-success uppercase pull-right">Нууц үг шинэчлэх холбоох илгээх</button>
    </div>
</form>
@endsection
