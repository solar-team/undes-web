@extends('AdminPanel::._layouts.master')
@section('style')
    <link href="/assets/chat/chat.css" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    <div class="chat-container">
        <div class="left">
            <div class="top">
                <input type="text" />
                <a href="javascript:;" class="search">
                    <i class="fa fa-search" ></i>
                </a>
            </div>
            <ul class="people">
                <li class="person" data-chat="person1">
                    <img src="http://s13.postimg.org/ih41k9tqr/img1.jpg" alt="" />
                    <span class="name">Мөнхжаргал</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">I was wondering...</span>
                </li>
                <li class="person active" data-chat="person2">
                    <img src="http://s3.postimg.org/yf86x7z1r/img2.jpg" alt="" />
                    <span class="name">Үүлэн солонго</span>
                    <span class="time">1:44 PM</span>
                    <span class="preview">Сайн байна уу багшаа</span>
                </li>
                <li class="person" data-chat="person3">
                    <img src="http://s3.postimg.org/h9q4sm433/img3.jpg" alt="" />
                    <span class="name">Louis CK</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">But we’re probably gonna need a new carpet.</span>
                </li>
                <li class="person" data-chat="person4">
                    <img src="http://s3.postimg.org/quect8isv/img4.jpg" alt="" />
                    <span class="name">Bo Jackson</span>
                    <span class="time">2:09 PM</span>
                    <span class="preview">It’s not that bad...</span>
                </li>

            </ul>
        </div>
        <div class="right">
            <div class="top"><span>To: <span class="name">Dog Woofson</span></span></div>
            <div class="chat" data-chat="person1">
                <div>
                    <div class="conversation-start">
                        <span>Today, 6:48 AM</span>
                    </div>
                    <div class="bubble you">
                        Hello,
                    </div>
                    <div class="bubble you">
                        it's me.
                    </div>
                    <div class="bubble you">
                        I was wondering...
                    </div>
                </div>
            </div>
            <div class="chat active-chat" data-chat="person2">
                <div>
                    <div class="conversation-start">
                        <span>Today, 5:38 PM</span>
                    </div>
                    <div class="bubble you">
                        Hello, can you hear me?
                    </div>
                    <div class="bubble you">
                        I'm in California dreaming
                    </div>
                    <div class="bubble me">
                        ... about who we used to be.
                    </div>
                    <div class="bubble me">
                        Are you serious?
                    </div>
                    <div class="bubble you">
                        When we were younger and free...
                    </div>
                    <div class="bubble you">
                        I've forgotten how it felt before
                    </div>
                    <div class="bubble you">
                        I've forgotten how it felt before
                    </div>
                    <div class="bubble you">
                        I've forgotten how it felt before
                    </div>
                    <div class="bubble you">
                        I've forgotten how it felt before
                    </div>
                </div>
            </div>
            <div class="chat" data-chat="person3">
                <div>
                    <div class="conversation-start">
                        <span>Today, 3:38 AM</span>
                    </div>
                    <div class="bubble you">
                        Hey human!
                    </div>
                    <div class="bubble you">
                        Umm... Someone took a shit in the hallway.
                    </div>
                    <div class="bubble me">
                        ... what.
                    </div>
                    <div class="bubble me">
                        Are you serious?
                    </div>
                    <div class="bubble you">
                        I mean...
                    </div>
                    <div class="bubble you">
                        It’s not that bad...
                    </div>
                    <div class="bubble you">
                        But we’re probably gonna need a new carpet.
                    </div>
                </div>
            </div>
            <div class="chat " data-chat="person4">
                <div>
                    <div class="conversation-start">
                        <span>Yesterday, 4:20 PM</span>
                    </div>
                    <div class="bubble me">
                        Hey human!
                    </div>
                    <div class="bubble me">
                        Umm... Someone took a shit in the hallway.
                    </div>
                    <div class="bubble you">
                        ... what.
                    </div>
                    <div class="bubble you">
                        Are you serious?
                    </div>
                    <div class="bubble me">
                        I mean...
                    </div>
                    <div class="bubble me">
                        It’s not that bad...
                    </div>
                </div>
            </div>
            <div class="write">
                <a href="javascript:;" class="write-link attach"></a>
                <input type="text" />
                <a href="javascript:;" class="write-link smiley"></a>
                <a href="javascript:;" class="write-link send"></a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/assets/chat/chat.js"></script>
@endsection