

    @if(Config::get('tp_config.tp_debug'))
        <link rel="stylesheet" href="http://localhost:3000/css/tp.css" type="text/css"/>
    @else
        <link rel="stylesheet" href="{{ URL::asset('shared/table-properties/css/tp.css') }}" type="text/css"/>
    @endif


    <style>
        .tp_header{
            display: none;
        }
        .danger {
            display: none !important;
        }
        .add-edit-form{
            margin-bottom: 10px !important;
        }
    </style>
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <title>{{ $pageTitle or 'ЕРХӨГ АРВАЙ ҮНДЭСНИЙ СҮЛЖЭЭ' }}</title>
            {{--<title>{{ $pageTitle or 'Solar Admin Panel' }}</title>--}}
            <meta content="" name="description" />
            <meta content="" name="author" />
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link href="/adminpanel/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="/adminpanel/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
            <link href="/adminpanel/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/adminpanel/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
            <!-- END GLOBAL MANDATORY STYLES -->
            <!-- BEGIN THEME GLOBAL STYLES -->
            <link href="/adminpanel/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
            <link href="/adminpanel/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
            <!-- END THEME GLOBAL STYLES -->
            <!-- BEGIN THEME LAYOUT STYLES -->
            <link href="/adminpanel/css/layout.min.css" rel="stylesheet" type="text/css" />
            <link href="/adminpanel/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />

            <!-- END THEME LAYOUT STYLES -->

            <!-- END HEAD -->
            <!--favicon start-->
            <link rel="apple-touch-icon" sizes="57x57" href="/assets/img/favicon/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="/assets/img/favicon/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/favicon/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/favicon/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicon/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicon/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="/assets/img/favicon/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="/assets/img/favicon/apple-touch-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/favicon/apple-touch-icon-180x180.png">
            <link rel="icon" type="image/png" href="/assets/img/favicon/favicon-32x32.png" sizes="32x32">
            <link rel="icon" type="image/png" href="/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
            <link rel="icon" type="image/png" href="/assets/img/favicon/favicon-96x96.png" sizes="96x96">
            <link rel="icon" type="image/png" href="/assets/img/favicon/favicon-16x16.png" sizes="16x16">
            <link rel="manifest" href="/assets/img/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#da532c">
            <meta name="msapplication-TileImage" content="/assets/img/favicon/mstile-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <!--favicon end-->

            @yield('style')

            <link href="/adminpanel/css/custom.min.css" rel="stylesheet" type="text/css" />
        </head>
        <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">



    <div id="solar-tp"></div>


    <script>
        window.setup = {!! json_encode($setup) !!};
    </script>
    @if($setup['googleMap'] == true)
        <script src="https://maps.googleapis.com/maps/api/js"></script>
    @endif





        <script src="/adminpanel/global/plugins/jquery.min.js" type="text/javascript"></script>


    <script type="text/javascript" charset="utf-8" src="{{ URL::asset('shared/ckeditor/ckeditor.js')}}"></script>
    @if(Config::get('tp_config.tp_debug'))
        {{--<script src="http://localhost:3000/js/dependencies.js"></script>--}}
        <script type="text/javascript" charset="utf-8"
                src="{{ URL::asset('shared/table-properties/js/dependencies.js')}}"></script>
        <script src="http://localhost:3000/js/tp.js"></script>
    @else
        <script type="text/javascript" charset="utf-8"
                src="{{ URL::asset('shared/table-properties/js/dependencies.js')}}"></script>
        <script type="text/javascript" charset="utf-8"
                src="{{ URL::asset('shared/table-properties/js/tp.js')}}"></script>
    @endif

        </body>

