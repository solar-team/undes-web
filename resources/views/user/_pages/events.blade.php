@extends('user._layouts.master')

@section('style')
    <link href="/users/css/user.css" rel="stylesheet" type="text/css" />

@endsection

@section('content')


<div id="binaryTeam">
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title" style="margin-top: 0">Таны багт: <b><span id="totalMembers"></span></b> хүн байна.
        <small>Багдаа хүн нэмхийг хүсэл, дурын хүн дээр дарж доор хүн элсүүлнэ үү</small>
    </h3>

    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div id="binarTreeShow">
        <div id="body">

        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="m_header"></h4>
                    </div>
                    <div class="modal-body">

                        <div id="m_body"></div>
                        <div id="loading" style="display: none">
                            <img  src="/users/img/loading-spinner-blue.gif" alt="">
                        </div>

                        <div id="register_form" class="hidden-form">
                            <hr>
                            <h4>Гишүүн нэмэх</h4>

                            <div class="row">
                                <div class="col-md-12">

                                    <iframe id="new_user_form" src="" frameborder="0" width="100%" height="1100px" onafterupdate="javascript:alert('event has been triggered');">

                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Хаах</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>





@endsection


@section('script')


    <script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script>
        window.binaryTree = {!! json_encode($binaryTree) !!};
        window.prefix = 'user';
    </script>
    <script src="/users/js/user.js"></script>


@endsection
