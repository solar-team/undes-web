var Login = function() {




  

    return {
        //main function to initiate the module
        init: function() {


            // init background slide images
            $('.login-bg').backstretch([
                "/auth/img/login/bg1.jpg",
                "/auth/img/login/bg2.jpg",
                "/auth/img/login/bg3.jpg"
                ], {
                  fade: 1000,
                  duration: 8000
                }
            );

         

        }

    };

}();

jQuery(document).ready(function() {
    Login.init();
});