
$(document).ready(function () {

    if(typeof(Storage) !== "undefined") {
    }

    else {
        alert("Sorry, your browser does not support web storage...");

    }

    var users = window.binaryTree;

    $("#totalMembers").text(users.length-1);

    var treeData = [];
    var nodeUser = {};

    var parenTlevel = users[0].tree_level;

    var status = 'disactive';
    if (users[0].active == 1) {
        status = 'normal';
    } else {
        status = 'disactive';
    }
    var userName = users[0].username;
    // var userName = users[0].firstname == '' ? users[0].username : users[0].firstname;

    var preChildR = getChild(users[0].id, 'r');
    var preChildL = getChild(users[0].id, 'l');
    var preChild = [];
    if(preChildR){
        preChild.push(preChildR);
    }
    if(preChildL){
        preChild.push(preChildL);
    }
    if(preChild.length >= 1){
        nodeUser = {
            "status": status,
            "pside": users[0].pside,
            "name": userName,
            "user_level":0,
            "userid":users[0].id,
            "phone": users[0].phone,
            "children":preChild
        }
    } else {
        nodeUser = {
            "status": status,
            "pside": users[0].pside,
            "name": userName,
            "userid":users[0].id,
            "user_level":0,
            "phone": users[0].phone,
        }
    }
    treeData.push(nodeUser);

    var lineY = 0;
    var foundMoreThan = false;
    var mostXLess = 0;
    var mostXMore = 0;
// ************** Generate the tree diagram	 *****************
    var m = [20, 120, 20, 120],
        w = 1280 - m[1] - m[3],
        h = 800 - m[0] - m[2],
        i = 0;

    var tree = d3.layout.tree()
        .size([h, w]).nodeSize([50,])
        .separation(function separation(a, b) {
            return a.parent == b.parent ? 2 : 2;
        });

    var diagonal = d3.svg.diagonal()
        .projection(function (d) {
            return [d.x, d.y];
        });


    var svg = d3.select("#body").append("svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .call(zm = d3.behavior.zoom().scaleExtent([1, 6]).on("zoom", redraw))
        .append("g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");


    var myLine = svg.append("svg:line")
        .attr("x1", 0)
        .attr("y1", lineY)
        .attr("x2", w + m[1] + m[3])
        .attr("y2", lineY)
        .style("stroke", "rgb(248,0,0)");


    root = treeData[0];


    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    // root.children.forEach(collapse);


    update(root);

    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * 100;
        });

        // Declare the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function (d) {
                return d.id || (d.id = ++i);
            });

        // Enter the nodes.
        var nodeEnter = node.enter().append("g")
            .attr("class", getClass)
            .attr("transform", function (d) {
                if(d.x < mostXLess){
                    mostXLess = d.x-100;

                    myLine.remove();
                    myLine = svg.append("svg:line")
                        .attr("x1", mostXLess)
                        .attr("y1", lineY)
                        .attr("x2", mostXMore)
                        .attr("y2", lineY)
                        .style("stroke", "rgb(248,0,0)");
                }
                if(d.x > mostXMore){
                    mostXMore = d.x+100;
                    myLine.remove();
                    myLine = svg.append("svg:line")
                        .attr("x1", mostXLess)
                        .attr("y1", lineY)
                        .attr("x2", mostXMore)
                        .attr("y2", lineY)
                        .style("stroke", "rgb(248,0,0)");

                }
                return "translate(" + d.x + "," + d.y + ")";
            })
            .on("click", click);


        nodeEnter.append("circle")
            .attr("r", 10)
            .style("fill", "#fff");


        nodeEnter.append("text")
            .attr("y", function (d) {
                return d.children || d._children ? -18 : 18;
            })
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .text(function (d) {
                return d.name;
            })
            .style("fill-opacity", 1);

        // Declare the links…
        var link = svg.selectAll("path.link")
            .data(links, function (d) {
                return d.target.id;
            });

        // Enter the links.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", diagonal);

    }

// Toggle children on click.
    function click(d) {
        console.log(d)

        $('#m_header').text('');
        $('#m_body').html('');

        $("#register_form").removeClass('show-form');

        $('#m_header').text(d.name);
        $('#m_body').html("<div>" +d.id+ d.name + "<br/>" + d.phone + "</div>");


        if (d.children) {

            if (d.children && d.children.length >= 2) {
                $("#register_form").removeClass('show-form');
            } else {
                $('#loading').css('display', 'block');
                var child_pside = '';
                if(d.children[0].pside == 'r'){
                    child_pside='l';
                }else{
                    child_pside='r';
                }
                $("#new_user_form").attr("src", "/user/tp/new/"+d.userid+"/"+child_pside+"/#/add?_k=kvwuk1");
                setTimeout(function(){
                    $('#loading').css('display', 'none');
                    $("#register_form").addClass('show-form');



                }, 2100);


            }
        } else {

            if (d.status == 'disactive')
                $("#register_form").removeClass('show-form');
            else{
                $('#loading').css('display', 'block');
                $("#new_user_form").attr("src", "/user/tp/new/"+d.userid+"/null/#/add?_k=kvwuk1");
                setTimeout(function(){
                    $('#loading').css('display', 'none');
                    $("#register_form").addClass('show-form');



                }, 2100);

            }

        }


        $("#myModal").modal('toggle');
    }

//Redraw for zoom


    function redraw() {
        svg.attr("transform",
            "translate(" + d3.event.translate + ")"
            + " scale(" + d3.event.scale + ")");
    }

    function getClass(d) {
        var level_varning = '';
        if(d.user_level == 6){
            level_varning = ' red_6';

            if(lineY <= d.y){

                lineY = d.y+20;



                myLine.remove();
                myLine = svg.append("svg:line")
                    .attr("x1", mostXLess)
                    .attr("y1", lineY)
                    .attr("x2", mostXMore)
                    .attr("y2", lineY)
                    .style("stroke", "rgb(248,0,0)");

                foundMoreThan=true;

            }


        }
        if (d.status == 'disactive') {
            return 'node disactive'+level_varning;
        } else if (d.status == 'active') {
            return 'node active-user'+level_varning;
        } else {
            return 'node'+level_varning;
        }
    }

    function getChild(parentId, pside) {

        var nodeUser_ = {};
        var i;
        for (i = 0; i < users.length; i++) {
            // console.log(users[i]);

            if(users[i].parent_id == parentId && users[i].pside == pside){


                var status_ = 'disactive';
                if (users[i].active == 1) {
                    status_ = 'normal';
                } else {
                    status_ = 'disactive';
                }
                var userName_ = users[i].username;
                // var userName_ = users[i].firstname == '' ? users[i].username : users[i].firstname;

                var preChildR_ = getChild(users[i].id, 'r');
                var preChildL_ = getChild(users[i].id, 'l');

                var preChild_ = [];

                if(preChildR_){
                    preChild_.push(preChildR_);
                }

                if(preChildL_){
                    preChild_.push(preChildL_);
                }

                var user_level = users[i].tree_level - parenTlevel;

                if(preChild_.length >= 1){
                    nodeUser_ = {
                        "status": status_,
                        "pside": users[i].pside,
                        "userid":users[i].id,
                        "name": userName_+", үе:"+user_level,
                        "user_level":user_level,
                        "phone": users[i].phone,
                        "children":preChild_
                    }
                } else {
                    nodeUser_ = {
                        "status": status_,
                        "userid":users[i].id,
                        "pside": users[i].pside,
                        "name": userName_+", үе:"+user_level,
                        "user_level":user_level,
                        "phone": users[i].phone,
                    }
                }

                return nodeUser_;
            }
        }


        return false;

    }

    if(foundMoreThan === false)
        myLine.remove();




    function displayStorageEvent(e) {
        if (e.key == 'addEditFromStatus') {
            console.log(e.newValue)

            if(e.newValue == 'ended'){
                $('#loading').css('display', 'none');
                $("#myModal").modal('toggle');
                window.location.reload();
            }
        }
    }

    window.addEventListener("storage", displayStorageEvent, true);

});

